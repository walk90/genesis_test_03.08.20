# Genesis_test_03.08.20

- Create two instance:
Input secrets for your aws account and public key

cd terraform && terraform init && terraform apply

- Wordpress:
Add private key used in terraform to dir ansible.

Change vars (private ip) #ansible-vault edit vars/vars.yml  and inventory-file hosts(public ip). Make inventory file actual.

cd ansible && ansible-playbook --ask-vault-pass -i hosts wordpress.yml && ansible-playbook --ask-vault-pass -i hosts db.yml
