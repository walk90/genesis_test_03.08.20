variable access_key {
  default = ""
}

variable secret_key {
  default = ""
}

variable region {
  default = "eu-central-1"
}

variable ami {
  default = "ami-0d359437d1756caa8"
}

variable instance_type {
  default = "t2.micro"
}

variable public_key {
  default = ""
}

